#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
 
 
int int_cmp(const void *a, const void *b) 
{ 
    const int *ia = (const int *)a; 
    const int *ib = (const int *)b;
    return *ia  - *ib; 
	 
} 
 


 
 
void sortint() 
{ 
    int numbers[] = { 7, 3, 4, 1, -1, 23, 12, 43, 2, -4, 5 }; 
    int len = sizeof(numbers)/sizeof(int);
 
    printf("----Integer sorting----");
 
    printf("\n");
    int i;
 
    for(i=0; i<len; i++) 
        printf("%d,", numbers[i]);
 
printf("\n");
 
  
    qsort(numbers, len, sizeof(int), int_cmp);
 
   
    
 
    for(i=0; i<len; i++) 
        printf("%d,", numbers[i]);
 
  printf("\n");
} 
 

int cstring_cmp(const void *a, const void *b) 
{ 
    const char **ia = (const char **)a;
    const char **ib = (const char **)b;
    return strcmp(*ia, *ib);
	
} 
 

 

void sortstr() 
{ 
    char *strings[] = { "Zorro", "Alex", "Celine", "Bill", "Forest", "Dexter" };
    int len = sizeof(strings) / sizeof(char*);
 
   
    printf("----String sorting----");
	printf("\n");
    
    int i;
 
    for(i=0; i<len; i++) 
        printf("%s,", strings[i]);
 
    printf("\n");
 
    qsort(strings, len, sizeof(char *), cstring_cmp);
 
   
    
 
    for(i=0; i<len; i++) 
        printf("%s,", strings[i]);
 
    printf("\n");
} 
 
 int main() 
{ 
    
    sortint();
    sortstr();
   
    return 0;
} 
 