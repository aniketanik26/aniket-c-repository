#include<stdio.h>
#include<conio.h>
#include<string.h>
#include<stdlib.h>

struct node
{
char word[20];
char mean[100];
struct node* next;
};

struct node* head=NULL;
int choice;
void insert();
void display();
void search();
void delete();
struct node* create(char word[],char mean[])
{
struct node* newnode=malloc(sizeof(struct node));
strcpy(newnode->word,word);
strcpy(newnode->mean,mean);
newnode->next=NULL;
return newnode;
}

void main( )
{
   int ch; 

    while ( 1 )
    {
        
        printf ( "Dictionary\n" );
		printf ( "-------------\n" ) ;
        printf ( "1.Add Word.\n" ) ;
		printf ( "2.Delete Word.\n" ) ;
		printf ( "3.Search Word.\n" ) ;
		printf ( "4.Show Dictionary.\n" ) ;
        printf ( "5.Exit.\n" ) ;
        printf ( "Your Choice\n") ;
        scanf ( "%d", &ch ) ;

        switch ( ch )
        {
            case 1 :

                insert();
                break ;
			case 2:delete();
					break;
					
            case 3:search();
					break;
			
			case 4:display();
					break;
            case 5 :

                
                exit (0) ;

            default :

                printf ( "\nWrong Choice" ) ;
        }
    }
}


void insert()
{
struct node* temp=head;
char input1[20];
char input2[100];
printf("Enter your word to be added---:\n");
scanf("%s",input1);

while(temp!=NULL)
{
if(strcmp(input1,temp->word)==0)
{
printf("Same word cannot be added again.\n");
return;
}
temp=temp->next;

}

printf("Enter meaning of the word entered---:\n");
scanf("%s",input2);

struct node* final= create(input1,input2);

if(head==NULL)
{
head=final;
}
else
{
	temp=head;
while(temp->next!=NULL)
{
temp=temp->next;
}
temp->next=final;
}


}



void display()
{
	struct node* temp=head;
	int i=1;
	printf("**************************************************\n");
	printf("Displaying the dictionary\n");
	while(temp!=NULL)
	{	
		
		printf("%d. %s   -----  %s\n",i,temp->word,temp->mean);
		temp=temp->next;
		i=1+1;
	}
	printf("*****************************************\n");
	printf("\n");
}

void delete()
{
	char input[20];
	printf("Enter word to delete---:\n");
	scanf("%s",input);
	struct node* temp=head;
	struct node* prev;
	int result=strcmp(input,temp->word);
	
	if(result==0)
	{
		head=temp->next;
		free(temp);
	}
	else
	{
		while(temp->next!=NULL)
		{
			prev=temp;
			temp=temp->next;
			result=strcmp(input,temp->word);
			if(result==0)
			{
				prev->next=temp->next;
				free(temp);
				break;
			}
		}
	}
	
}

void search()
{
	char input[20];
	struct node* temp=head;
	printf("Enter the word you want to search--:\n");
	scanf("%s",&input);
	while(temp!=NULL)
	{
		int result=strcmp(temp->word,input);
		if(result==0)
		{
			printf("The element is found.\n");
			return;
		}
		temp=temp->next;
	}
	printf("The element is not found.\n");
}









